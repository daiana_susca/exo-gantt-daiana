package com.nespresso.exercise.gantt;


import java.util.ArrayList;
import java.util.List;

public class Gantt {

    private static final String STATUS_VALUE = ".";
    
    private List<Task> tasks;
    private int ganttDuration = 0;

    public Gantt() {
        tasks = new ArrayList<Task>();
    }

    protected void addTask(String name, int start, int end) {
        Task task = new Task(name, start, end);

        tasks.add(task);
        updateGanttDuration();
    }

    protected void addTask(String name, int start, int end, String dependency) {
        Task task = new Task(name, start, end);
        task.addDependencyTask(getTaskByName(dependency));

        tasks.add(task);
        updateGanttDuration();
    }

    private Task getTaskByName(String dependency) {
        for (Task task : tasks) {
            if(task.getName().equals(dependency)) {
                return task;
            }
        }
        return null;
    }


    protected String showPlanFor(String taskName) {
        for (Task task : tasks) {
            if(task.getName().equals(taskName)) {
                String waiting = getPlanWaitingSegment(task);
                String duration = getPlanDurationSegment(task);

                return waiting + duration;
            }
        }
        return "";
    }

    private String getPlanWaitingSegment(Task task) {
        StringBuilder waitingSegment = new StringBuilder();
        if(task.getStart() > 0) {
            for (int i = 0; i < task.getStart(); i++) {
                waitingSegment.append(STATUS_VALUE);
            }
        }

        return waitingSegment.toString();
    }


    private String getPlanDurationSegment(Task task) {
        StringBuilder plan = new StringBuilder();
        for(int i = 0; i < task.getDuration(); i++) {
           plan.append(task.getName().substring(0,1).toUpperCase());
        }

        if(isNotLast(task)) {
            addExtendTimeline(plan, task);
        }

        return plan.toString();
    }

    private boolean isNotLast(Task task) {
        if (tasks.get(tasks.size()-1).equals(task)) {
            return false;
        }
        return true;
    }

    private void addExtendTimeline(StringBuilder plan, Task task) {
        int extended = ganttDuration - task.getDuration();
        for (int i=0; i<extended; i++) {
            plan.append(STATUS_VALUE);
        }
    }

    private void updateGanttDuration() {
        int longest = 0;
        Task longestTask = null ;
        for (Task task : tasks) {
            if (task.getDuration() > longest) {
                longest = task.getDuration();
                longestTask = task;
            }
        }

        ganttDuration = longestTask.getDuration() + longestTask.getStart();
    }


}

package com.nespresso.exercise.gantt;


import java.util.ArrayList;
import java.util.List;

public class Task {

    private String name;
    private int start;
    private int duration;
    private List<Task> dependentTasks;



    public Task(String name, int start, int duration) {
        this.name = name;
        this.start = start;
        this.duration = duration;

        dependentTasks = new ArrayList<Task>();
    }

    public void addDependencyTask(Task task) {
        if (task != null) {
            dependentTasks.add(task);
        }

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
